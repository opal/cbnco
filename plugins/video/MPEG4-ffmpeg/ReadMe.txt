OPAL MPEG4 plugin
-----------------

This plugin allows OPAL to use MPEG4 as a high-resolution SIP video codec.
Resolutions up to 704x480 have been tested. The plugin uses the ffmpeg
library, also known as libavcodec, to encode and decode MPEG4 data.

The plugin should work with recent versions of the ffmpeg library. The
following versions are known to work:

  * Subversion checkout r8997 (2007-05-11)

Success or failure reports with other versions would be appreciated -
please send to the OpenH323 mailing list (openh323@lists.sourceforge.net).

To detect decoder errors caused by packet loss, the plugin uses some of
ffmpeg's private data structures. Consequently you will need a copy of the
ffmpeg source tree when building the plugin.


Building ffmpeg
---------------

Download the latest ffmpeg sources from SVN following the instructions
at http://ffmpeg.mplayerhq.hu/ .

Configure and build ffmpeg:
       $ ./configure --enable-shared
       $ make

Many Linux distributions ship with a version of ffmpeg - check for
/usr/lib/libavcodec.so*. It is best not to replace these with the new
versions you've compiled. Instead, copy the new versions to a separate
directory:
       $ su
       # mkdir /usr/local/opal-mpeg4
       # cp -p libavutil/libavutil.so* /usr/local/opal-mpeg4
       # cp -p libavcodec/libavcodec.so* /usr/local/opal-mpeg4


Building the plugin
-------------------

When you configure OPAL, pass the argument
       --with-mpeg4-ffmpeg=<path to your ffmpeg source directory>


Using the plugin
----------------

Use the LD_LIBRARY_PATH environment variable to tell the plugin where to
find the ffmpeg libraries at runtime. For example, if you installed
libavutil.so.49 and libavcodec.so.51 to /usr/local/opal-mpeg4,
export LD_LIBRARY_PATH=/usr/local/opal-mpeg4 before running your
application.


Compatibility with the H.263 plugin
-----------------------------------

The H.263 video plugin loads a patched version of ffmpeg 0.4.7, using the
PWLIBPLUGINDIR environment variable (default /usr/local/lib/pwlib) to find
libavcodec.so. Put the patched libavcodec.so for H.263 in
/usr/local/lib/pwlib, put the new libavutil and libavcodec in
/usr/local/opal-mpeg4, set LD_LIBRARY_PATH appropriately, and there should
be no conflict between the plugins.


Interoperability
----------------

The plugin does not use a standardized framing for MPEG4 in RTP, so
it is not interoperable.
