#
# opal_inc.mak
#
# Make symbols include file for Open Phone Abstraction library
#
# Copyright (c) 2001 Equivalence Pty. Ltd.
#
# The contents of this file are subject to the Mozilla Public License
# Version 1.0 (the "License"); you may not use this file except in
# compliance with the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS IS"
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
# the License for the specific language governing rights and limitations
# under the License.
#
# The Original Code is Open Phone Abstraction library.
#
# The Initial Developer of the Original Code is Equivalence Pty. Ltd.
#
# Contributor(s): ______________________________________.
#
# $Log$
# Revision 1.14  2007/06/02 13:38:37  dsandras
# Fixed build on linux which was broken due to partial commit of the RFC4175
# code.
#
# Revision 1.13  2006/10/10 07:18:18  csoutheren
# Allow compilation with and without various options
#
# Revision 1.12  2006/10/04 06:19:08  csoutheren
# Fixed SRTP configuration for Linux
#
# Revision 1.11  2006/06/27 13:50:23  csoutheren
# Patch 1375137 - Voicetronix patches and lid enhancements
# Thanks to Frederich Heem
#
# Revision 1.10  2006/05/30 12:04:09  hfriederich
# Enabling to build H.460
#
# Revision 1.9  2006/04/20 16:56:41  hfriederich
# Changes to allow compilation of H.224/H.281 code as well as to disable H.224 entirely
#
# Revision 1.8  2006/04/17 00:06:27  csoutheren
# Allow ILBC codec to be disabled and detect if not present
#
# Revision 1.7  2006/01/30 02:23:16  csoutheren
# First cut at fixing problem with speex libraries
#
# Revision 1.6  2005/12/06 06:34:10  csoutheren
# Added configure support for Sangoma and empty LID source and header files
#
# Revision 1.5  2005/07/30 07:39:26  csoutheren
# Added support for compiling new IAX2 code
#
# Revision 1.4  2005/01/15 09:19:39  csoutheren
# Fixed compile problems and IXJ link problems
#
# Revision 1.3  2004/12/05 18:14:20  dsandras
# Cleanups.
#
# Revision 1.2  2004/04/25 08:33:42  rjongbloed
# Removed detection of various LIDs as now in configure
#
# Revision 1.1  2004/03/16 04:27:18  csoutheren
# Initial version
#
# Revision 2.4  2002/09/11 05:55:40  robertj
# Fixed double inclusion of common.mak
# Added more directories to search to find pwlib
#
# Revision 2.3  2002/04/19 01:24:30  robertj
# Changed /usr/include to SYSINCDIR helps with X-compiling, thanks Bob Lindell
#
# Revision 2.2  2002/03/15 10:51:53  robertj
# Fixed problem with recursive inclusion on make files.
#
# Revision 2.1  2002/02/06 09:39:37  rogerh
# Look for telephony.h in the place where the FreeBSD port puts it
#
# Revision 2.0  2001/07/27 15:48:24  robertj
# Conversion of OpenH323 to Open Phone Abstraction Library (OPAL)
#

OPALDIR=@OPALDIR@
PWLIBDIR=@PWLIBDIR@

LIBDIRS += $(OPALDIR)

ifdef LIBRARY_MAKEFILE
include $(PWLIBDIR)/make/unix.mak
else
ifdef NOTRACE
OBJDIR_SUFFIX := n
endif
include $(PWLIBDIR)/make/ptlib.mak
endif

OPAL_SRCDIR = $(OPALDIR)/src
OPAL_INCDIR = $(OPALDIR)/include
OPAL_LIBDIR = $(OPALDIR)/lib

OPAL_SIP          = @OPAL_SIP@
OPAL_H323         = @OPAL_H323@
OPAL_IAX2         = @OPAL_IAX2@
OPAL_LID	  = @OPAL_LID@
OPAL_IVR	  = @OPAL_IVR@
OPAL_H224         = @OPAL_H224@
OPAL_SYSTEM_SPEEX = @OPAL_SYSTEM_SPEEX@
OPAL_SRTP	  = @OPAL_SRTP@
OPAL_RFC4175	  = @OPAL_RFC4175@
HAS_LIBSRTP	  = @HAS_LIBSRTP@

H323_H460         = @H323_H460@

ifdef NOTRACE
STDCCFLAGS += -DPASN_NOPRINTON -DPASN_LEANANDMEAN
OPAL_SUFFIX = n
else
STDCCFLAGS += -DPTRACING
RCFLAGS	   += -DPTRACING
OPAL_SUFFIX = $(OBJ_SUFFIX)
endif


OPAL_BASE  = opal_$(PLATFORM_TYPE)_$(OPAL_SUFFIX)
OPAL_FILE  = lib$(OPAL_BASE)$(LIB_TYPE).$(LIB_SUFFIX)

LDFLAGS	    += -L$(OPAL_LIBDIR)
LDLIBS	    := -l$(OPAL_BASE)$(LIB_TYPE) $(LDLIBS)

STDCCFLAGS  += -I$(OPAL_INCDIR)

ifeq ($(HAS_LIBSRTP), 1)
ENDLDLIBS       += -lsrtp
endif

$(TARGET) :	$(OPAL_LIBDIR)/$(OPAL_FILE)

ifndef LIBRARY_MAKEFILE

ifdef DEBUG
$(OPAL_LIBDIR)/$(OPAL_FILE):
	$(MAKE) -C $(OPALDIR) debug
else
$(OPAL_LIBDIR)/$(OPAL_FILE):
	$(MAKE) -C $(OPALDIR) opt
endif

libs :: $(OPAL_LIBDIR)/$(OPAL_FILE)

endif


# End of file

